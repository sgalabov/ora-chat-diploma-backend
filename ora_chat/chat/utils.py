from rest_framework import status
from rest_framework.response import Response

from chat.models import Channel, Message, Reaction, OraUser


def parseHeaders(user):
	access_token = 'Bearer ' + user.access_token		
	headers = {'Authorization': access_token}

	return headers


def checkUserInChannel(channel, user_id):
	for key, value in channel.items():
		if key == 'members':
			for member in value:
				for key, value in member.items():
					if key == 'id' and value == user_id:
						return True

	return False



