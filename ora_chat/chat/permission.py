from chat.models import Channel, Message, Reaction, OraUser

class PermissionHelper():
	@staticmethod
	def checkUserExists(user_id):
		user = OraUser.objects.filter(ora_id = user_id).first()
		if not user:
			return False

		return True

	@staticmethod
	def checkChannelExists(channel_id):
		channel = Channel.objects.filter(id = channel_id).first()
		if not channel:
			return False

		return True

	@staticmethod
	def checkMessageExists(message_id):
		message = Message.objects.filter(id = message_id).first()
		if not message:
			return False

		return True

	@staticmethod
	def checkReactionExists(reaction_id):
		reaction = Reaction.objects.filter(id = reaction_id).first()
		if not reaction:
				False

		return True
