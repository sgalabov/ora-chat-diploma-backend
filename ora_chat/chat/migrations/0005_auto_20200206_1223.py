# Generated by Django 2.2.3 on 2020-02-06 12:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0004_message_reactions'),
    ]

    operations = [
        migrations.CreateModel(
            name='OraUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ora_id', models.IntegerField()),
                ('username', models.CharField(max_length=200)),
                ('full_name', models.CharField(max_length=500)),
                ('profile_picture', models.CharField(max_length=1000)),
            ],
        ),
        migrations.RemoveField(
            model_name='channel',
            name='owner',
        ),
        migrations.AddField(
            model_name='channel',
            name='created_in',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_by',
            field=models.ForeignKey(null=True, on_delete=False, to='chat.OraUser'),
        ),
        migrations.AlterField(
            model_name='reaction',
            name='created_by',
            field=models.ForeignKey(null=True, on_delete=False, to='chat.OraUser'),
        ),
    ]
