# Generated by Django 2.2.3 on 2020-02-06 14:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('chat', '0005_auto_20200206_1223'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orauser',
            name='username',
        ),
        migrations.AddField(
            model_name='orauser',
            name='user',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
