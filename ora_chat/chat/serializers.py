from django.contrib.auth.models import User

from rest_framework import serializers

from chat.models import Channel, Message, Reaction, OraUser


class OraUserSerializer(serializers.Serializer):
	id = serializers.IntegerField()
	ora_id = serializers.IntegerField()
	full_name = serializers.CharField()
	profile_picture = serializers.CharField()


class ChannelSerializer(serializers.ModelSerializer):
	members = OraUserSerializer(many = True, read_only = True)

	class Meta:
		model = Channel
		fields = ('id', 'name', 'description', 'created_in', 'members')
		read_only_fields = ('id', 'members', 'created_in')
		

class ReactionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Reaction
		fields = ('id', 'name')
		read_only_fields = ('id',)


class MessageSerializer(serializers.ModelSerializer):
	created_in = ChannelSerializer(read_only = True)
	created_by = OraUserSerializer(read_only = True)
	reactions = ReactionSerializer(many = True, read_only = True)

	class Meta:
		model = Message
		fields = ('id', 'content', 'created_in', 'created_by','reactions')
		read_only_fields = ('id',)

