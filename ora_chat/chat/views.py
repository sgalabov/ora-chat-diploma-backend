import requests, json

from django.shortcuts import render
from django.conf import settings
from django.contrib.auth.models import User

from werkzeug.security import gen_salt

from rest_framework import status, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from rest_framework.authtoken.models import Token

from chat.models import Channel, Message, Reaction, OraUser
from chat.serializers import ChannelSerializer, MessageSerializer, ReactionSerializer
from chat.pagination import MessagePagination
from chat.permission import PermissionHelper
from chat.utils import parseHeaders, checkUserInChannel
# Create your views here.

class PrepareOAuthUri(APIView):
	def post(self, request, format = None):
		auth_uri = 'https://ora.pm/authorize?client_id=' + str(settings.CLIENT_ID) + '&redirect_uri=' + str(settings.REDIRECT_URI) + '&response_type=code'

		return Response({'uri': auth_uri}, status = status.HTTP_201_CREATED)


class GenerateAuthCredentials(APIView):
	def post(self, request, format = None):
		data = {}
		data['code'] = request.data['code']
		data['client_id'] = settings.CLIENT_ID
		data['client_secret'] = settings.CLIENT_SECRET
		data['redirect_uri'] = settings.REDIRECT_URI
		data['grant_type'] = 'authorization_code'

		response = requests.post('https://api.ora.pm/oauth/token', data = data)

		access_token = response.json().get('access_token')
		refresh_token = response.json().get('refresh_token')

		headers = {'Authorization': 'Bearer ' + access_token}
		response = requests.get('https://api.ora.pm/user/current', headers = headers)
		response = response.json()

		user, created = User.objects.get_or_create(username = response['username'], email = response['email'])
		oraUser, created = OraUser.objects.get_or_create(ora_id = response['id'], defaults = {
			'full_name': response['full_name'],
			'profile_picture': response['profile_picture'],
			'user_id': user.id
		})

		oraUser.access_token = access_token
		oraUser.refresh_token = refresh_token
		oraUser.save()

		token, created = Token.objects.get_or_create(user = user)
		print(token.key)
		return Response({'user': response, 'token': token.key}, status = status.HTTP_201_CREATED)

# dovqne na refresh token refresha-a, s podavane na nova zaqvka, ako api-to failne

# dovanqne na middleware za tokena

class UserOrganizations(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)
	
		response = requests.get('https://api.ora.pm/organizations', headers = headers)
		if response.status_code == 200:
				return Response({'organizations': response.json()['data']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class Organization(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, organization_id: int,  format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)

		response = requests.get('https://api.ora.pm/organizations', headers = headers)
		if response.status_code == 200:
			organizations = response.json()['data']
			for organization in organizations:
				if organization['id'] == organization_id:
					return Response({'organization': organization}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class OrganizationMembers(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, organization_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)
		
		response = requests.get('https://api.ora.pm/organizations/' + str(organization_id) + '/members', headers = headers)
		if response.status_code == 200:
			return Response({'members': response.json()['data']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class OrganizationProjects(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)

		response = requests.get('https://api.ora.pm/projects', headers = headers)
		if response.status_code == 200:
			return Response({'projects': response.json()['data']}, status = status.HTTP_200_OK)
	
		return Response(status = status.HTTP_400_BAD_REQUEST)


class Project(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, project_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)

		response = requests.get('https://api.ora.pm/projects/' + str(project_id), headers = headers)
		if response.status_code == 200:
			return Response({'project': response.json()['data']}, status = status.HTTP_200_OK)
			
		return Response(status = status.HTTP_400_BAD_REQUEST)


class Tasks(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, project_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)

		response = requests.get('https://api.ora.pm/projects/' + str(project_id) + '/tasks', headers = headers)
		if response.status_code == 200:
			return Response({'tasks': response.json()['data']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class TaskList(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, project_id: int, task_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)
		list_id = request.data.get('list_id')

		response = requests.get('https://api.ora.pm/projects/' + str(project_id) + '/lists/' + str(list_id), headers = headers)
		if response.status_code == 200:
			return Response({'list': response.json()['list']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class TaskComments(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, project_id: int, task_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		headers = parseHeaders(user)

		response = requests.get('https://api.ora.pm/projects/' + str(project_id) + '/tasks/' + str(task_id) + '/comments', headers = headers)
		if response.status_code == 200:
			return Response({'comments': response.json()['data']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class Channels(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, organization_id: int, format = None):	
		channels = Channel.objects.filter(created_in = organization_id, is_direct_channel = False)	
		serialized_channels = ChannelSerializer(channels, many = True).data

		starred_channels = []
		for channel in serialized_channels:
			if checkUserInChannel(channel, request.user.id):
				starred_channels.append(channel)

		return Response({'channels': starred_channels}, status = status.HTTP_200_OK)

	def post(self, request, organization_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		channel = ChannelSerializer(data = request.data)
		channel.is_valid(raise_exception = True)

		channel.save(created_in = organization_id, members = [user])
		return Response(channel.data, status = status.HTTP_200_OK)

	def put(self, request, channel_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id):
			channel = Channel.objects.filter(id = channel_id).first()

			new_name = request.data.get('name', None)
			if new_name:
				channel.name = new_name

			channel.save()
			channel = ChannelSerializer(channel).data
			return Response(channel, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)
		
	def delete(self, request, channel_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id):
			channel = Channel.objects.filter(id = channel_id).first()
			channel.delete()

			return Response(status = status.HTTP_200_OK)
		
		return Response(status = status.HTTP_400_BAD_REQUEST)


class ChannelMembers(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, channel_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id):
			channel = Channel.objects.filter(id = channel_id).first()

			return Response({'members': ChannelSerializer(channel).data['members']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)

	def post(self, request, channel_id: int, format = None):
		user_id = request.data.get('user_id')

		if PermissionHelper.checkChannelExists(channel_id) and PermissionHelper.checkUserExists(user_id):
			channel = Channel.objects.filter(id = channel_id).first()
			oraUser = OraUser.objects.filter(ora_id = user_id).first()
		
			channel.members.add(oraUser)
			channel.save()
			return Response({'members': ChannelSerializer(channel).data['members']}, status = status.HTTP_201_CREATED)
	
		return Response(status = status.HTTP_400_BAD_REQUEST)

	def delete(self, request, channel_id: int, member_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id) and PermissionHelper.checkUserExists(member_id):
			channel = Channel.objects.filter(id = channel_id).first()
			oraUser = OraUser.objects.filter(ora_id = member_id).first()
			
			channel.members.remove(oraUser)
			channel.save()
			return Response({'members': ChannelSerializer(channel).data['members']}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class Messages(generics.ListAPIView):
	permission_classes = (IsAuthenticated,)
	pagination_class = MessagePagination
	pagination_class.page_size = 20

	def get(self, request, channel_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id):
			channel = Channel.objects.filter(id = channel_id).first()
			messages = MessageSerializer(Message.objects.filter(created_in = channel), many = True).data
			
			pag_messages = self.get_paginated_response(data = self.paginate_queryset(messages)).data
			return Response({'messages': pag_messages, 'channel_id': channel.id}, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)

	def post(self, request, channel_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id):
			channel = Channel.objects.filter(id = channel_id).first()
			user = OraUser.objects.filter(user_id = request.user.id).first()
			message = MessageSerializer(data = request.data)
			message.is_valid(raise_exception = True)

			message.save(created_in = channel, created_by = user)
			return Response(message.data, status = status.HTTP_200_OK)
		
		return Response(status = status.HTTP_400_BAD_REQUEST)

	def put(self, request, channel_id: int, message_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id) and PermissionHelper.checkMessageExists(message_id):
			channel = Channel.objects.filter(id = channel_id).first()
			message = Message.objects.filter(id = message_id).first()
		
			new_content = request.data.get('content', None)
			if new_content:
				message.content = new_content

			message.save()
			message = MessageSerializer(message).data
			return Response(message, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)

	def delete(self, request, channel_id: int, message_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id) and PermissionHelper.checkMessageExists(message_id):
			message = Message.objects.filter(id = message_id).first()
		
			message.delete()
			return Response(status = status.HTTP_200_OK)
		
		return Response(status = status.HTTP_400_BAD_REQUEST)


class MessageReaction(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request, channel_id: int, message_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id) and PermissionHelper.checkMessageExists(message_id):
			channel = Channel.objects.filter(id = channel_id).first()
			message = Message.objects.filter(id = message_id).first()
			user = OraUser.objects.filter(user_id = request.user.id).first()

			reaction = ReactionSerializer(data = request.data)
			reaction.is_valid(raise_exception = True)

			reaction = reaction.save(created_by = user)
			message.reactions.add(reaction)
			return Response(MessageSerializer(message).data, status = status.HTTP_201_CREATED)

		return Response(status = status.HTTP_400_BAD_REQUEST)

	def delete(self, request, channel_id: int, message_id: int, reaction_id: int, format = None):
		if PermissionHelper.checkChannelExists(channel_id) and PermissionHelper.checkMessageExists(message_id) and PermissionHelper.checkReactionExists(reaction_id):
			channel = Channel.objects.filter(id = channel_id).first()
			message = Message.objects.filter(id = message_id).first()
			reaction = Reaction.objects.filter(id = reaction_id).first()

			reaction.delete()
			return Response(MessageSerializer(message).data, status = status.HTTP_200_OK)

		return Response(status = status.HTTP_400_BAD_REQUEST)


class DirectChannels(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, organization_id: int, format = None):
		direct_channels = Channel.objects.filter(is_direct_channel = True, created_in = organization_id)
		serialized_channels = ChannelSerializer(direct_channels, many = True).data

		starred_channels = []
		for channel in serialized_channels:
			if checkUserInChannel(channel, request.user.id):
				starred_channels.append(channel)

		return Response({'channels': starred_channels}, status = status.HTTP_200_OK)

	def post(self, request, organization_id: int, format = None):
		user = OraUser.objects.filter(user_id = request.user.id).first()
		other_user = OraUser.objects.filter(ora_id = request.data['user_id']).first()

		channel = Channel.objects.create(name = other_user.full_name, description = "Private chat with " + other_user.full_name, is_direct_channel = True, created_in = organization_id)
		channel.members.add(user)
		channel.members.add(other_user)

		channel.save()
		return Response(ChannelSerializer(channel).data, status = status.HTTP_200_OK)
