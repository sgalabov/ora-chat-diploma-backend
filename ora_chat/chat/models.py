from django.db import models

from django.contrib.auth.models import User


class OraUser(models.Model):
	user_id = models.IntegerField()
	ora_id = models.IntegerField()
	full_name = models.CharField(max_length = 500)
	profile_picture = models.CharField(max_length = 1000)
	access_token = models.CharField(max_length = 200)
	refresh_token = models.CharField(max_length = 200)


class Channel(models.Model):
	name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 500)
	created_at = models.DateTimeField(auto_now_add = True)
	created_in = models.IntegerField()
	members = models.ManyToManyField(OraUser)
	is_direct_channel = models.BooleanField(default = False)


class Reaction(models.Model):
	name = models.CharField(max_length = 100)
	created_by = models.ForeignKey(OraUser, on_delete = False)


class Message(models.Model):
	content = models.CharField(max_length = 1000)
	created_at = models.DateTimeField(auto_now_add = True)
	created_by = models.ForeignKey(OraUser, on_delete = False)
	created_in = models.ForeignKey(Channel, on_delete = models.CASCADE)
	reactions = models.ManyToManyField(Reaction)
