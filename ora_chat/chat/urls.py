from django.urls import path

from chat.views import *

urlpatterns = [
	path('authenticate', PrepareOAuthUri.as_view()),
	path('oauth/complete', GenerateAuthCredentials.as_view()),
	path('organizations', UserOrganizations.as_view()),
	path('organizations/<int:organization_id>', Organization.as_view()),
	path('organizations/<int:organization_id>/members', OrganizationMembers.as_view()),
	path('projects', OrganizationProjects.as_view()),
	path('projects/<int:project_id>', Project.as_view()),
	path('projects/<int:project_id>/tasks', Tasks.as_view()),
	path('projects/<int:project_id>/tasks/<int:task_id>/list', TaskList.as_view()),
	path('projects/<int:project_id>/tasks/<int:task_id>/comments', TaskComments.as_view()),
	path('organizations/<int:organization_id>/channels', Channels.as_view()),
	path('organizations/<int:organization_id>/direct_channels', DirectChannels.as_view()),
	path('channels/<int:channel_id>', Channels.as_view()),
	path('channels/<int:channel_id>/members', ChannelMembers.as_view()),
	path('channels/<int:channel_id>/members/<int:member_id>', ChannelMembers.as_view()),
	path('channels/<int:channel_id>/messages', Messages.as_view()),
	path('channels/<int:channel_id>/messages/<int:message_id>', Messages.as_view()),
	path('channels/<int:channel_id>/messages/<int:message_id>/reactions', MessageReaction.as_view()),
	path('channels/<int:channel_id>/messages/<int:message_id>/reactions/<int:reaction_id>', MessageReaction.as_view()),
]