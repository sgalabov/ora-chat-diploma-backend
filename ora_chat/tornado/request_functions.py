import json, requests

url = "http://localhost:8001"

def format(r, type):
	return json.dumps({ 'type' : type, 'data' : r.json() })

def parseHeader(token):
	return {'Authorization': 'Token ' + token}

def authenticate(socket, data, type, token):
	r = requests.post(url + "/authenticate", data = data)
	socket.write_message(format(r, type))

def completeAuth(socket, data, type, token):
	r = requests.post(url + "/oauth/complete", data = data)
	socket.write_message(format(r, type))

def getOrganizations(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/organizations", headers = headers)
	socket.write_message(format(r, type))

def getOrganization(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/organizations/" + str(data['organization_id']), headers = headers)
	socket.write_message(format(r, type))

def getOrganizationMembers(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/organizations/" + str(data['organization_id']) + '/members', headers = headers)
	socket.write_message(format(r, type))

def getProjects(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/projects", headers = headers)
	socket.write_message(format(r, type))

def getProject(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/projects/" + str(data['project_id']), headers = headers)
	socket.write_message(format(r, type))

def getTasks(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/projects/" + str(data['project_id']) + "/tasks", headers = headers)
	socket.write_message(format(r, type))

def getTaskList(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/projects/" + str(data['project_id']) + "/tasks/" + str(data['task_id']) + "/list", headers = headers, data = data)
	socket.write_message(format(r, type))

def getTaskComments(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/projects/" + str(data['project_id']) + "/tasks/" + str(data['task_id']) + "/comments", headers = headers)
	socket.write_message(format(r, type))

def getChannels(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/organizations/" + str(data['organization_id']) + "/channels", headers = headers)
	socket.write_message(format(r, type))

def createChannel(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.post(url + "/organizations/" + str(data['organization_id']) + "/channels", data = data, headers = headers)
	socket.write_message(format(r, type))

def updateChannel(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.put(url + "/channels/" + str(data['channel_id']), data = data, headers = headers)
	socket.write_message(format(r, type))

def deleteChannel(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.delete(url + "/channels/" + str(data['channel_id']), headers = headers)
	socket.write_message(format(r, type))

def channelMembers(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.get(url + "/channels/" + str(data['channel_id']) + "/members", headers = headers)
	socket.write_message(format(r, type))

def addChannelMember(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.post(url + "/channels/" + str(data['channel_id']) + "/members", data = data, headers = headers)
	socket.write_message(format(r, type))

def removeChannelMember(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.delete(url + "/channels/" + str(data['channel_id']) + "/members/" + str(data['user_id']), data = data, headers = headers)
	socket.write_message(format(r, type))

def getMessages(socket, data, type, token):
	headers = parseHeader(token)
	if 'page' in data:
		page = "page=" + str(data['page'])
	else:
		page = "page=1"

	r = requests.get(url + "/channels/" + str(data['channel_id']) + "/messages?" + page, headers = headers)
	socket.write_message(format(r, type))

def createMessage(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.post(url + "/channels/" + str(data['channel_id']) + "/messages", data = data, headers = headers)
	socket.write_message(format(r, type))
		
def updateMessage(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.post(url + "/channels/" + str(data['channel_id']) + "/messages/" + str(data['message_id']), data = data, headers = headers)
	socket.write_message(format(r, type))
		
def deleteMessage(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.delete(url + "/channels/" + str(data['channel_id']) + "/messages/" + str(data['message_id']), headers = headers)
	socket.write_message(format(r, type))

def addReaction(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.post(url + "/channels/" + str(data['channel_id']) + "/messages/" + str(data['message_id']) + "/reactions", data = data, headers = headers)
	socket.write_message(format(r, type))

def removeReaction(socket, data, type, token):
	headers = parseHeader(token)
	r = requests.delete(url + "/channels/" + str(data['channel_id']) + "/messages/" + str(data['message_id']) + "/reactions/" + str(data['reaction_id']), data = data, headers = headers)
	socket.write_message(format(r, type))