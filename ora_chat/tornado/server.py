import tornado, json, requests, uuid
from tornado import websocket, ioloop
from tornado.ioloop import IOLoop
from tornado.ioloop import PeriodicCallback
from tornado.escape import json_encode

from request_functions import *

class WebSocketHandler(websocket.WebSocketHandler):
	is_active = False

	def __init__(self, application, request, **kwargs):
		super(WebSocketHandler, self).__init__(application, request, **kwargs)

	def check_origin(self, origin):
		return True

	def open(self):
		print("Connected")
		self.is_active = True

	def on_message(self, data):
		json_data = json.loads(data)

		handler = self.application.settings['events'][json_data['type']]
		handler(self, json_data['data'], json_data['type'], json_data['token'])
	
	def on_close(self):
		print("Disconnected")
		self.is_active = False

def main():
	application = tornado.web.Application([
		(r'/ws', WebSocketHandler),
	])

	application.settings['events'] = {
		'authenticate': authenticate,
		'completeAuth': completeAuth,
		'organizations': getOrganizations,
		'organization': getOrganization,
		'organizationMembers': getOrganizationMembers,
		'projects': getProjects,
		'project': getProject,
		'tasks': getTasks,
		'taskList': getTaskList,
		'taskComments': getTaskComments,
		'channels': getChannels,
		'channelMembers': channelMembers,
		'addChannelMember': addChannelMember,
		'removeChannelMember': removeChannelMember,
		'channel': createChannel,
		'updateChannel': updateChannel,
		'deleteChannel': deleteChannel,
		'messages': getMessages,
		'message': createMessage,
		'updateMessage': updateMessage,
		'deleteMessage': deleteMessage,
		'addReaction': addReaction,
		'removeReaction': removeReaction,
	}

	application.listen(8888)
	IOLoop.current().start()


if __name__ == "__main__":
	print("started")
	main()
